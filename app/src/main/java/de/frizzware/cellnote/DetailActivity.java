package de.frizzware.cellnote;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;

import com.activeandroid.query.Select;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by Frederik on 21.06.2014.
 */
public class DetailActivity extends Activity {

    public static final String EXTRA_CELLNOTE_ID = "cellnoteid";

    private ImageView mapView;
    private AutoCompleteTextView location;
    private EditText title, description;
    private Button save, discard;

    private ArrayAdapterNoFilter locationAdapter;
    private ArrayList<Address> searchResults;
    private Address selectedAddress;

    private Bitmap mapImage;

    private CellNote currentNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initUI();

        if(getIntent().getLongExtra(EXTRA_CELLNOTE_ID, 0) != 0){
            //Load and display the current CellNote
            currentNote = new Select()
                    .from(CellNote.class)
                    .where("Id = ?", getIntent().getLongExtra(EXTRA_CELLNOTE_ID, 0))
                    .executeSingle();

            if(currentNote == null) return;
            mapView.setImageBitmap(BitmapFactory.decodeFile(currentNote.mapfile));
            location.setText(currentNote.address);
            title.setText(currentNote.title);
            description.setText(currentNote.text);
        }
    }

    private void initUI(){
        mapView = (ImageView) findViewById(R.id.imageViewDetailMap);
        location = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewDetail);
        title = (EditText) findViewById(R.id.editTextDetailTitle);
        description = (EditText) findViewById(R.id.editTextDetailDescription);
        save = (Button) findViewById(R.id.buttonSave);
        discard = (Button) findViewById(R.id.buttonDiscard);

        locationAdapter = new ArrayAdapterNoFilter(getApplicationContext(), R.layout.auto_complete_item);
        location.setAdapter(locationAdapter);

        location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedAddress = searchResults.get(i);
                downloadMap(selectedAddress.getLatitude(), selectedAddress.getLongitude());
            }
        });
        location.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {}

            @Override
            public void afterTextChanged(Editable editable) {
                updateLocationList(editable.toString());
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNote();
            }
        });
        discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                discardNote();
            }
        });
    }

    private void saveNote(){

        if(currentNote == null) {
            if (selectedAddress == null) {
                //TODO No location selected
                return;
            }

            if (title.getText().length() < 3) {
                //TODO Title too short
                return;
            }

            currentNote = new CellNote();
            currentNote.title = title.getText().toString();
            currentNote.text = description.getText().toString();

            updateAddress();

            currentNote.timestamp = System.currentTimeMillis();
            currentNote.mapfile = saveMap(currentNote.timestamp + "");
            currentNote.save();
            currentNote.register(getApplicationContext());

        }else{
            currentNote.title = title.getText().toString();
            currentNote.text = description.getText().toString();

            if(selectedAddress != null){
                updateAddress();
            }

            //Register again when location was changed
            currentNote.unregister(getApplicationContext());
            currentNote.save();
            currentNote.register(getApplicationContext());
        }

        finish();
    }

    private void updateAddress(){
        if (selectedAddress.getAddressLine(1) != null) {
            currentNote.address = selectedAddress.getAddressLine(0) + ", " + selectedAddress.getAddressLine(1);
        }else{
            currentNote.address = selectedAddress.getAddressLine(0);
        }

        currentNote.latitude = selectedAddress.getLatitude();
        currentNote.longitude = selectedAddress.getLongitude();
        currentNote.radius = 250f;
    }

    private void discardNote(){
        if(currentNote != null){
            currentNote.delete();
            currentNote.unregister(getApplicationContext());
        }

        finish();
    }

    private String saveMap(String id){
        String outPath = Environment.getExternalStorageDirectory() + "/Android/data/de.frizzware.cellnote/maps/";
        File outputDir = new File(outPath);

        if(!outputDir.exists()){
            outputDir.mkdirs();
        }

        File outputFile = new File(outputDir, id + ".tmp");
        FileOutputStream out = null;

        try {
            out = new FileOutputStream(outputFile);
            mapImage.compress(Bitmap.CompressFormat.PNG, 90, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.flush();
                out.close();
            } catch(Exception ignore){}
        }

        return outputFile.getAbsolutePath();
    }

    private void downloadMap(double latitude, double longitude){
        LocationUtils.downloadMap(latitude, longitude, new LocationUtils.MapCallback() {
            @Override
            public void onMapReceived(Bitmap map) {
                mapImage = map;
                reloadMap();
            }

            @Override
            public void onError() {

            }
        });
    }

    private void reloadMap(){
        final Animation out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.out);
        final Animation in = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.in);

        out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation) {
                mapView.setImageBitmap(mapImage);
                mapView.startAnimation(in);
            }

            @Override
            public void onAnimationRepeat(Animation animation) { }
        });

        mapView.startAnimation(out);
    }

    private void updateLocationList(String search){
        LocationUtils.getAddresses(getApplicationContext(), search, new LocationUtils.LocationCallback() {

            @Override
            public void onAddressesReceived(ArrayList<Address> addresses) {
                searchResults = addresses;
                locationAdapter.clear();
                for(Address tmp : addresses){
                    locationAdapter.add(tmp.getAddressLine(0) + ", " + tmp.getAddressLine(1));
                }
                locationAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError() {

            }
        });
    }

    class ArrayAdapterNoFilter extends ArrayAdapter<String> {

        public ArrayAdapterNoFilter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        private final NoFilter NO_FILTER = new NoFilter();

        @Override
        public Filter getFilter() {
            return NO_FILTER;
        }

        private class NoFilter extends Filter {
            protected FilterResults performFiltering(CharSequence prefix) {
                return new FilterResults();
            }

            protected void publishResults(CharSequence constraint, FilterResults results) {
                //Do nothing
            }
        }
    }

}
