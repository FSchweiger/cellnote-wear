package de.frizzware.cellnote;

import android.app.Activity;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    private ListView listView;

    private CellNoteAdapter mAdapter;
    private ArrayList<CellNote> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent showDetail = new Intent(getApplicationContext(), DetailActivity.class);
                CellNote selected = (CellNote) mAdapter.getItem(i);
                showDetail.putExtra(DetailActivity.EXTRA_CELLNOTE_ID, selected.getId());
                startActivity(showDetail);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        items = new ArrayList<CellNote>();
        items = CellNote.all(CellNote.class);
        items = LocationUtils.calculateDistances(this, items);
        mAdapter = new CellNoteAdapter(this, items);
        listView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_add) {
            startActivity(new Intent(this, DetailActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}