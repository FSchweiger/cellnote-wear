package de.frizzware.cellnote;

import android.content.Context;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.File;
import java.util.List;

/**
 * Created by Frederik on 20.06.2014.
 */

@Table(name = "CellNotes")
public class CellNote extends Model implements Comparable<CellNote>{

    @Column(name = "Title")
    public String title;
    @Column(name = "Text")
    public String text;
    @Column(name = "Address")
    public String address;
    @Column(name = "Mapfile")
    public String mapfile;

    @Column(name = "Checked")
    public boolean isChecked;

    @Column(name = "Latitude")
    public double latitude;
    @Column(name = "Longitude")
    public double longitude;
    @Column(name = "Radius")
    public float radius;

    public float distance;

    @Column(name = "Timestamp")
    public long timestamp;

    public CellNote(){
        super();
    }

    @Override
    public void delete() {
        super.delete();
        File map = new File(mapfile);
        if(map.exists()){
            map.delete();
        }
    }

    @Override
    public int compareTo(CellNote note) {
        if(distance < note.distance){
            return -1;
        }else if(distance > note.distance){
            return 1;
        }
        return 0;
    }

    public void register(Context ctx){
        LocationUtils.registerLocationAlert(ctx, this, NotificationUtils.getShowNotificationPendingIntent(ctx, getId()));
    }

    public void unregister(Context ctx){
        LocationUtils.unregisterLocationAlert(ctx, NotificationUtils.getShowNotificationPendingIntent(ctx, getId()));
    }
}
