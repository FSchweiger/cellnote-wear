package de.frizzware.cellnote;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preview.support.v4.app.NotificationManagerCompat;

import com.activeandroid.query.Select;

/**
 * Created by Frederik on 20.06.2014.
 */
public class NotificationIntentReceiver extends BroadcastReceiver {

    public static final String
            ACTION_SHOW_NOTIFICATION = "de.frizzware.cellnote.ACTION_SHOW_NOTIFICATION",
            ACTION_DISMISS_CELLNOTE = "de.frizzware.cellnote.ACTION_DISMISS_CELLNOTE",
            ACTION_REPEAT_CELLNOTE  = "de.frizzware.cellnote.ACTION_REPEAT_CELLNOTE",
            ACTION_NEW_CELLNOTE     = "de.frizzware.cellnote.ACTION_NEW_CELLNOTE";

    @Override
    public void onReceive(Context context, Intent intent) {
        int notificationID = intent.getIntExtra(NotificationUtils.EXTRA_NOTIFICATION_ID, 0);
        NotificationManagerCompat manager = NotificationManagerCompat.from(context);

        if(intent.getAction().equals(ACTION_DISMISS_CELLNOTE)){
            long cellNoteID = intent.getLongExtra(NotificationUtils.EXTRA_CELLNOTE_ID, 0);

            CellNote cellNote = new Select()
                    .from(CellNote.class)
                    .where("Id = ?", cellNoteID)
                    .executeSingle();

            cellNote.isChecked = true;
            cellNote.save();

            manager.cancel(notificationID);
        }

        if(intent.getAction().equals(ACTION_REPEAT_CELLNOTE)){
            manager.cancel(notificationID);
        }

        if(intent.getAction().equals(ACTION_NEW_CELLNOTE)){
            Intent newIntent = new Intent(context, DetailActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(newIntent);
        }

        if(intent.getAction().equals(ACTION_SHOW_NOTIFICATION)){
            long cellNoteID = intent.getLongExtra(NotificationUtils.EXTRA_CELLNOTE_ID, 0);

            CellNote cellNote = new Select()
                    .from(CellNote.class)
                    .where("Id = ?", cellNoteID)
                    .executeSingle();

            if(cellNote == null) return;

            NotificationUtils.showNotification(context, cellNote);
        }
    }

}